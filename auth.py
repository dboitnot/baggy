from log import log
import random
import string
import hashlib
import time

from datetime import timedelta
from datetime import datetime

from config import conf


class TokenService(object):
    def __init__(self):
        log.debug("Generating new secret")
        self.secret = self.random_string(256)

    @staticmethod
    def random_string(size):
        return ''.join(random.choice(string.letters + string.digits) for _ in range(size))

    def digest(self, ets, salt):
        return hashlib.sha512("%d.%s.%s" % (ets, salt, self.secret)).hexdigest()

    @staticmethod
    def gen_exp():
        d = conf.extend_interval.default({'days': 1})
        delta = timedelta(**d)
        ret = datetime.now() + delta
        if conf.cutoff_hour.is_defined:
            ret = ret.replace(hour=conf.cutoff_hour.value, minute=0, second=0, microsecond=0)
        return ret

    def gen_token(self, expiration=None):
        if expiration is None:
            expiration = self.gen_exp()
        ets = int(time.mktime(expiration.timetuple()))
        salt = self.random_string(8)
        dig = self.digest(ets, salt)
        return "%d.%s.%s" % (ets, salt, dig)

    def check_token(self, token):
        parts = token.split('.')
        if len(parts) != 3:
            log.warn("Invalid token: %s", token)
            return None

        (ets, salt, dig) = parts
        try:
            ets = int(ets)
        except ValueError:
            log.warn("Invalid timestamp on token: %s", ets)
            return None

        if dig != self.digest(ets, salt):
            log.warn("Hash mismatch: %s", token)
            return None

        expires = datetime.fromtimestamp(ets)
        if expires < datetime.now():
            log.debug("Token has expired timestamp: %s", expires)
            return None
        return expires


tokenService = TokenService()

if __name__ == '__main__':
    ts = tokenService.gen_exp()
    print ts
