import mimetypes
import sys
import time
import traceback

import flask
from flask import Flask, request, send_file, redirect
# from gevent import monkey
from gevent.wsgi import WSGIServer

from auth import tokenService
from config import conf, ConfigError
from group import groupService
from log import log
from mail import mailService

# The monkey patch breaks other services. Disabling for now.
# monkey.patch_all()

app = Flask(__name__)


@app.route('/')
def root():
    if groupService.ip_allowed(request.remote_addr):
        return links_page()
    else:
        return request_form()


@app.route('/global.css')
def global_css():
    return template('global.css'), 200, {'Content-Type': 'text/css; charset=utf-8'}


@app.route('/logo')
def logo():
    l = conf.logo
    if l.is_defined:
        l = l.value
        (mime_type, _) = mimetypes.guess_type(l)
        print "mime_type:", mime_type
        return send_file(l, mime_type)


@app.route('/form_input', methods=['POST'])
def form_input():
    to_addr = request.form['email']
    if mailService.is_valid_address(to_addr):
        exp = tokenService.gen_exp()
        token = tokenService.gen_token(exp)
        log.debug("Sending token to %s", to_addr)
        mailService.send_token(to_addr, token, exp)
        return mail_sent(to_addr)

    # Invalid address
    return request_form("The address '%s' is not valid." % to_addr)


@app.route('/validate', methods=['GET'])
def validate():
    token = request.args['token']
    expires = tokenService.check_token(token)
    if expires is None:
        return request_form("This token is expired or invalid.")

    for addr in [request.remote_addr] + request.headers.getlist("X-Forwarded-For"):        
        groupService.extend(addr, expires)

    # Token is valid, redirect to next validation URL or to the links page
    if "idx" in request.args:
        idx = int(request.args["idx"]) + 1
    else:
        idx = 1

    base_urls = conf.base_urls
    if idx >= len(base_urls):
        log.debug("Redirecting to links page: %s", base_urls[0])
        return redirect(base_urls[0])

    url = conf.validation_url(token, idx)
    log.debug("Redirecting to next validation URL (idx=%d): %s", idx, url)
    return redirect(url)


@app.route('/request')
def request_form(msg=None):
    return template('email_form.html', message=msg)


@app.route('/links', methods=['GET'])
def force_links():
    return links_page()


@app.route('/health_check')
def health_check():
    return "Still running."


@app.errorhandler(Exception)
def handle_generic_error(ex):
    log.error("An unexpected error occurred: %s\n%s", ex, traceback.format_exc())


def links_page(msg=None):
    return template('links.html', message=msg, links=conf.links.default([]))


def mail_sent(to_addr):
    return "Mail sent to: %s" % to_addr


def template(t, **kwargs):
    defaults = {'conf': conf}
    for k in defaults.keys():
        if k not in kwargs.keys():
            kwargs[k] = defaults[k]
    return flask.render_template(t, **kwargs)


# Launch using Gevent
def start_gevent_multi():
    def start_port(port):
        log.info("Starting with gevent WSGI server on port %d" % port)
        http_server = WSGIServer(('0.0.0.0', port), app)
        http_server.start()
        return http_server

    ports = conf.listen_ports
    listeners = [start_port(p) for p in ports]

    try:
        while True:
            time.sleep(0.5)
    except:
        log.info("Caught %s - shutting down" % sys.exc_info()[0])
        for l in listeners:
            l.stop()


def start_gevent_single():
    ports = conf.listen_ports
    if len(ports) > 1:
        raise ConfigError("multiple listen_ports not (yet) supported")

    port = ports[0]
    log.info("Starting with gevent wsgi server on port %d" % port)
    from gevent.wsgi import WSGIServer
    http_server = WSGIServer(('0.0.0.0', port), app)
    http_server.serve_forever()


def start():
    log.info("Starting Baggy with base_urls:\n   %s" % "\n   ".join(conf.base_urls))
    start_gevent_single()


if __name__ == '__main__':
    start()
