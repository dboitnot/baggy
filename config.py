import argparse
import os.path

import yaml
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler


class ConfigError(Exception):
    pass


class ConfigNode(object):
    def __init__(self, path, data):
        self.path = path
        self.data = data

    def default(self, d=None):
        if self.data is None:
            return d
        return self.data

    @property
    def required(self):
        if self.data is None:
            raise ConfigError("missing required configuration: %s" % self.path)
        return self.data

    @property
    def value(self):
        return self.data

    @property
    def is_defined(self):
        return self.data is not None

    def is_dict(self, obj=None):
        if obj is None:
            obj = self.data
        return isinstance(obj, dict)

    def as_dict(self):
        if self.data is None:
            return {}
        if not self.is_dict():
            raise ValueError("config path is not a dict: %s" % self.path)
        return self.data

    def as_list(self, default=None):
        if self.data is None:
            return default
        if not isinstance(self.data, list):
            return [self.data]
        return self.data

    def get_child(self, *keys):
        if len(keys) < 1:
            raise ValueError("get_child requires at least one key")
        for k in keys:
            node = self.__getattr__(k)
            if node.is_defined:
                return node
        return ConfigNode(keys[0], None)

    def __getattr__(self, item):
        if not self.is_dict():
            raise ValueError("config path '%s' is not a dict" % self.path)
        if len(self.path) < 1:
            new_path = item
        else:
            new_path = "%s.%s" % (self.path, item)
        if item not in self.data.keys():
            return ConfigNode(new_path, None)
        return ConfigNode(new_path, self.data[item])


class EventHandler(FileSystemEventHandler):
    def on_modified(self, event):
        conf.mark_dirty()


class ConfigService(object):
    def __init__(self):
        self.root = None
        p = argparse.ArgumentParser(description="Baggy whitelist manager")
        p.add_argument('--config', help="path to configuration file", metavar="path", default="/etc/baggy_conf.yml")
        args = p.parse_args()
        self.config_path = args.config

    def mark_dirty(self):
        self.root = None

    def reload(self):
        with open(self.config_path, 'r') as fp:
            self.root = ConfigNode("", yaml.load(fp))

    def start(self):
        config_dir = os.path.dirname(self.config_path)
        if len(config_dir) < 1:
            config_dir = "."
        event_handler = EventHandler()
        observer = Observer()
        observer.schedule(event_handler, config_dir)
        observer.start()

    def __getattr__(self, item):
        if self.root is None:
            self.reload()
        return getattr(self.root, item)

    #
    # App-specific methods. These are defined here because they are shared
    # across multiple modules.
    #

    @property
    def listen_ports(self):
        return self.root.get_child("listen_ports", "listen_port").as_list([5000])

    @property
    def base_urls(self):
        return self.root.get_child("base_urls", "base_urls")\
            .as_list(["http://localhost:%d" % p for p in self.listen_ports])

    def validation_url(self, token, idx=0):
        base = self.base_urls[idx]
        return "%s/validate?token=%s&idx=%d" % (base, token, idx)


conf = ConfigService()

if __name__ == '__main__':
    import time
    conf.start()
    while True:
        time.sleep(1)
