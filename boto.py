import boto3
import functools

from config import conf

resource = functools.partial(boto3.resource, region_name=conf.region_name.required)
client = functools.partial(boto3.client, region_name=conf.region_name.required)
