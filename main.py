import webapp
from group import groupService
from config import conf
from log import log

if __name__ == '__main__':
    log.info("Starting Baggy")

    log.debug("Starting group service")
    groupService.start()

    log.debug("Starting configuration monitor service")
    conf.start()

    log.debug("Starting webapp service")
    webapp.start()
