import threading
import traceback
import itertools
import functools
from datetime import datetime

import boto
from log import log
from config import conf

ISO8601_FORMAT = '%Y-%m-%dT%H:%M:%S'


def gen_tag_key(ip):
    return "expire-%s" % ip


class GroupService(object):
    def __init__(self):
        self.ec2 = boto.resource('ec2')

    @property
    def sg_id(self):
        return conf.security_group_id.required

    @property
    def interval(self):
        return conf.expire_check_interval.default(300)

    def group_handle(self):
        ret = self.ec2.SecurityGroup(self.sg_id)
        ret.load()
        return ret

    def allowed_ips(self):
        log.debug("Loading IP list")
        chain = itertools.chain.from_iterable([x['IpRanges'] for x in self.group_handle().ip_permissions if x['IpProtocol'] == '-1'])
        return [ip for ip, bits in [x['CidrIp'].split('/') for x in chain] if bits == '32']

    def ip_allowed(self, ip):
        return ip in self.allowed_ips()

    def extend(self, ip, expires):
        ts = expires.strftime(ISO8601_FORMAT)
        log.info("Extending IP %s until %s", ip, ts)
        boto.client('ec2').create_tags(Resources=[self.sg_id], Tags=[{'Key': gen_tag_key(ip), 'Value': ts}])
        if not self.ip_allowed(ip):
            self.group_handle().authorize_ingress(IpProtocol='-1', CidrIp="%s/32" % ip)

    def expire(self, ip):
        log.info("Expiring IP: %s", ip)
        self.group_handle().revoke_ingress(IpProtocol='-1', CidrIp="%s/32" % ip)
        boto.client('ec2').delete_tags(Resources=[self.sg_id], Tags=[{'Key': gen_tag_key(ip)}])

    def process_expirations(self):
        log.debug("Checking expirations")
        g = self.group_handle()
        for ip in self.allowed_ips():
            log.debug("Processing IP: %s", ip)
            tag_key = gen_tag_key(ip)
            tags = [x for x in g.tags if x['Key'] == tag_key]
            if len(tags) < 1:
                log.warn("No expiration tag found for IP: %s", ip)
                self.expire(ip)
                continue
            tag_value = tags[0]['Value']
            try:
                exp_time = datetime.strptime(tag_value, ISO8601_FORMAT)
                if exp_time <= datetime.now():
                    log.debug("IP %s expired at %s", ip, tag_value)
                    self.expire(ip)
            except ValueError:
                log.warn("Invalid expiration value for %s: %s", ip, tag_value)
                self.expire(ip)
        log.debug("Finished checking expirations")

    def timer_callback(self):
        try:
            self.process_expirations()
        except:
            log.error("Unexpected error processing expirations: \n%s", traceback.format_exc())
        threading.Timer(self.interval, self.timer_callback).start()

    def start(self):
        log.debug("Starting expiration daemon")
        threading.Timer(0, self.timer_callback).start()

groupService = GroupService()

if __name__ == '__main__':
    groupService.extend('10.1.1.1', datetime.now())
    groupService.start()
