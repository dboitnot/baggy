import logging.config
import sys

from config import conf

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '[ %(levelname)7s ] %(module)s: %(message)s'
        }
    },
    'handlers': {
        'stdout': {
            'class': 'logging.StreamHandler',
            'stream': sys.stdout,
            'formatter': 'verbose',
        }
    },
    'loggers': {
        'app-logger': {
            'handlers': ['stdout'],
            'level': logging.DEBUG,
            'propagate': True,
        },

        # Override the Flask/Werkzeug logger
        'werkzeug': {
            'handlers': ['stdout'],
            'level': logging.DEBUG,
            'propagate': True,
        }
    }
}

# Add Papertrail logging
if conf.papertrail.is_defined:
    c = conf.papertrail
    sender = c.sender.default('baggy')
    app_name = c.app_name.default('baggy')
    host = c.host.required
    port = c.port.required

    LOGGING['formatters']['papertrail'] = {
        'format': '%%(asctime)s %s %s: [ %%(levelname)7s ] %%(module)s: %%(message)s' % (sender, app_name),
        'datefmt': '%Y-%m-%dT%H:%M:%S',
    }
    LOGGING['handlers']['papertrail'] = {
        'class': 'logging.handlers.SysLogHandler',
        'address': (host, port),
        'formatter': 'papertrail'
    }
    for l in LOGGING['loggers'].keys():
        LOGGING['loggers'][l]['handlers'] = ['stdout', 'papertrail']

logging.config.dictConfig(LOGGING)
log = logging.getLogger("app-logger")
