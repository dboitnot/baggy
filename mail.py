import re

from datetime import datetime

import boto
from config import conf
from log import log

DEFAULT_BODY = """
You've requested access to a Baggy Gateway. To activate please use this URL:

  %(url)s

This invitation will remain valid until %(expires)s.
""".lstrip()


class MailService(object):
    def __init__(self):
        self.vex = re.compile("^[a-zA-Z._\-]+$")
        self.ses = boto.client('ses')

    @property
    def valid_domains(self):
        ret = conf.valid_domains.default([])
        if isinstance(ret, str):
            ret = [ret]
        if len(ret) < 1:
            log.error("valid_domains list is empty")
        return ret

    @property
    def from_addr(self):
        return conf.from_email.required

    @property
    def subject(self):
        return conf.email_subject.default('Baggy Access Link')

    @property
    def body_format(self):
        return conf.email_body.default(DEFAULT_BODY)

    @property
    def date_format(self):
        return conf.email_date_format.default("%Y-%m-%d %H:%M%z")

    def is_valid_address(self, a):
        parts = a.split("@")
        if len(parts) != 2:
            return False
        if parts[1] not in self.valid_domains:
            return False
        return self.vex.match(parts[0]) is not None

    def send_token(self, to_addr, token, expires):
        expires = expires.strftime(self.date_format)
        url = conf.validation_url(token)
        body = self.body_format % {
            'url': url,
            'expires': expires
        }

        self.ses.send_email(
            Source=self.from_addr,
            Destination={'ToAddresses': [to_addr]},
            Message={
                'Subject': {'Data': self.subject},
                'Body': {'Text': {'Data': body}}
            }
        )


mailService = MailService()

if __name__ == '__main__':
    mailService.send_token("boitnott@sigcorp.com", "test", datetime.now())
